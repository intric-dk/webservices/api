package main

import (
	"log"

	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"

	"gitlab.com/intric-dk/webservices/api/pkg/app"
	"gitlab.com/intric-dk/webservices/api/pkg/util"
)

// version is the application version.
var version string

// Initialize application.
func init() {
	// Configure logging.
	log.SetFlags(log.LstdFlags | log.LUTC)

	// Configure application version.
	if version == "" {
		version = "dev"
	}
	util.SetVersion(version)
}

func main() {
	// Run HTTP server.
	app.Serve()
}
