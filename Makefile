.PHONY: serve build test update download docker
REMOTE = gitlab.com
REGISTRY = registry.gitlab.com
NAMESPACE = intric-dk/webservices
APP = api
VERSION ?= dev
ARCH ?= amd64
BRANCH = $(shell git rev-parse --abbrev-ref HEAD)

# Set GOARCH and GOARM values for cross-compilation.
ifeq ($(ARCH),amd64)
	GOARCH = amd64
endif

ifeq ($(ARCH),armv7hf)
	GOARCH = arm
	GOARM = 7
endif

ifeq ($(ARCH),aarch64)
	GOARCH = arm64
endif

serve:
	CGO_ENABLED=0 GOARCH=$(GOARCH) GOARM=$(GOARM) go run -ldflags "-X main.version=$(VERSION)" cmd/api/main.go

build:
	CGO_ENABLED=0 GOARCH=$(GOARCH) GOARM=$(GOARM) go build -o ./$(APP)-$(ARCH) -ldflags "-s -w -X main.version=$(VERSION)" cmd/api/main.go

test:
	CGO_ENABLED=0 GOARCH=$(GOARCH) GOARM=$(GOARM) go test ./...

update:
	rm go.*
	go mod init $(REMOTE)/$(NAMESPACE)/$(APP)
	CGO_ENABLED=0 GOARCH=$(GOARCH) GOARM=$(GOARM) go test ./...
	go mod tidy

download:
	wget -q --show-progress https://$(REMOTE)/$(NAMESPACE)/$(APP)/-/jobs/artifacts/$(BRANCH)/raw/$(APP)-$(ARCH)?job=build-$(ARCH) -O $(APP)-$(ARCH)
	chmod +x $(APP)-$(ARCH)

docker:
	docker build --build-arg VERSION=$(VERSION) --build-arg ARCH=$(ARCH) -t $(REGISTRY)/$(NAMESPACE)/$(APP):$(VERSION)-$(ARCH) .
