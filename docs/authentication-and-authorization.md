# Authentication and authorization

This document describes authentication roles and processes. The API and all services that build on top of it, will further be referred to as **the platform**. Below, you may find an overview over the documents contents:

- [Authentication](#authentication)
  - [Identity types](#identity-types)
    - [User identity](#user-identity)
    - [Service identity](#service-identity)
  - [Authentication providers](#user-scopes)
    - [Global authentication provider](#global-authentication-provider)
    - [Organisation authentication provider](#organisation-authentication-provider)
- [Authorization](#authorization)
  - [Scheme](#scheme)

## Authentication

_Is the user who he claims to be?_

### Identity types

The platform distinguishes two types of identities for authentication, **users** and **services**.

#### User identity

User identities corresponds to a real-world user that interacts with the platform. User identities are always to be authenticated via external authentication providers. This can be done via protocols such as **OpenID Connect** or **OAuth 2.0**.

#### Service identity

Service accounts are used to authenticate applications against the platform. A service account does not correspond to a physical person. Services authenticate directly against the platform via an **API key**.

### Authentication providers

An authentication provider is an applicaiton that implements **OpenID Connect** or **OAuth 2.0**. It allows users to log in to the platform with an existing user account.

#### Global authentication providers

A global authentication provider can be used by platform admins, organisation users or customers of organisations to log on.

#### Organisation authentication providers

In some cases, especially in large enterprises, organisations have their own single sign-on solution. In this case, they may choose to configure authentication via their internal system. The authentication provider can be a company internal system that supports **OpenID Connect** or **OAuth 2.0**. If an organisation does not have such a system in place, they may select one or multiple [global authentication providers](#global-authentication-providers).

## Authorization

_Is the user allowed to access a certain resource?_

The authorization for the platform is done via roles, which are described more in detail [here](#default-roles). Roles are managed via [Casbin][casbin] as it allows to define a flexible autorization model across languages and platforms.

### Scheme

The configured scheme for this platform is based on role-based access control and extends it by adding field level access control. The model for Casbin looks like this:

```ini
[request_definition]
r = sub, org, vrb, res, fld

[policy_definition]
p = sub, org, vrb, res, fld, eft

[role_definition]
g = _, _
g2 = _, _, _

[policy_effect]
e = some(where (p.eft == allow)) && !some(where (p.eft == deny))

[matchers]
m = (g(r.sub, p.sub) || g2(r.sub, p.sub, r.org)) && \
    globMatch(r.res, p.res) && \
    globMatch(r.fld, p.fld) && \
    globMatch(r.vrb, p.vrb)
```

The table below explains the abbreviations used in the Casbin model.

| Abbreviation | Name         | Description                                                                                                                 |
| ------------ | ------------ | --------------------------------------------------------------------------------------------------------------------------- |
| sub          | Subject      | The user or service requesting access.                                                                                      |
| org          | Organisation | The organisation for which access is requested.                                                                             |
| vrb          | Verb         | The verb describing the type of access, which may be one of:<br />**list**, **create**, **read**, **update** or **delete**. |
| res          | Resource     | The resource for which access is requested.                                                                                 |
| fld          | Field        | A user or service requesting access.                                                                                        |
| eft          | Effect       | The effect of the policy. It can be either **allow** or **deny**.                                                           |

As the matchers show, resources, fields and verbs support [globs][globs].

[casbin]: https://casbin.org/en/
[globs]: https://en.wikipedia.org/wiki/Glob_(programming)
