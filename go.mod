module gitlab.com/intric-dk/webservices/api

go 1.14

require (
	cloud.google.com/go v0.58.0 // indirect
	github.com/casbin/casbin/v2 v2.7.2
	github.com/casbin/gorm-adapter/v2 v2.1.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/go-chi/jwtauth v4.0.4+incompatible
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/google/uuid v1.1.1
	github.com/gorilla/sessions v1.2.0
	github.com/gosimple/slug v1.9.0
	github.com/jinzhu/gorm v1.9.14
	github.com/joho/godotenv v1.3.0
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/markbates/goth v1.64.2
	github.com/stretchr/testify v1.6.1
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
