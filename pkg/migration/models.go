package migration

import (
	"log"

	"github.com/jinzhu/gorm"

	"gitlab.com/intric-dk/webservices/api/pkg/models"
)

// DB migrates the database models to the most recent version.
func DB(db *gorm.DB) {
	log.Println("Migrating database models ...")

	// Define database models to migrate.
	models := []interface{}{
		&models.AuthenticationProvider{},
		&models.Organisation{},
		&models.Project{},
		&models.User{},
	}

	// Migrate all models.
	for _, model := range models {
		err := db.AutoMigrate(model).Error
		if err != nil {
			log.Fatalf("error: migrating database model failed: %s", err)
		}
	}
}
