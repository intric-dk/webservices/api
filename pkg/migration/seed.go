package migration

import (
	"encoding/json"
	"log"
	"os"
	"strings"

	"github.com/jinzhu/gorm"

	"gitlab.com/intric-dk/webservices/api/pkg/models"
	"gitlab.com/intric-dk/webservices/api/pkg/util"
)

// SeedData describes the structure of the data,
// which is loaded into the database initially.
type SeedData struct {
	Authorization struct {
		Policies    []string `json:"policies"`
		GlobalRoles []string `json:"global_roles"`
	} `json:"authorization"`
	Users                   []models.User                   `json:"users"`
	AuthenticationProviders []models.AuthenticationProvider `json:"authentication_providers"`
}

// Seed will load data from a seed to prepopulate the database
// with important data before the server startup.
func Seed(db *gorm.DB, seed string) {
	// Parse the seed data.
	var seedData SeedData
	err := json.Unmarshal([]byte(seed), &seedData)
	if err != nil || &seedData == nil {
		log.Println("No seed data found!")
		return
	}
	log.Println("Seeding initial data ...")

	// Seed necessary policies and roles.
	seedAuthorization(&seedData)

	// Seed models.
	seedUsers(db, &seedData)
	seedAuthenticationProviders(db, &seedData)
}

// parse will open and parse a seed file.
func parse(filePath string) (*SeedData, error) {
	// Open file.
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}

	// Decode JSON file contents.
	var seedData SeedData
	err = json.NewDecoder(file).Decode(&seedData)
	if err != nil {
		return nil, err
	}

	return &seedData, nil
}

// seedAuthorization creates some basic access rules.
func seedAuthorization(seedData *SeedData) {
	// Get casbin enforcer.
	enforcer := util.Enforcer()

	// Add policies.
	for i := range seedData.Authorization.Policies {
		_, err := enforcer.AddNamedPolicy("p", strings.Split(seedData.Authorization.Policies[i], ", "))
		if err != nil {
			log.Fatalf("error: seeding policies failed")
		}
	}

	// Add roles.
	for i := range seedData.Authorization.GlobalRoles {
		_, err := enforcer.AddNamedGroupingPolicy("g", strings.Split(seedData.Authorization.GlobalRoles[i], ", "))
		if err != nil {
			log.Fatalf("error: seeding grouping policies failed")
		}
	}

	// Load policies.
	err := enforcer.LoadPolicy()
	if err != nil {
		log.Fatalf("error: synchronizing authorization failed: %s", err)
	}
}

// seedUsers synchronizes user information from the seed data.
func seedUsers(db *gorm.DB, seedData *SeedData) {
	// Ensure that all records exist and have the desired information.
	for _, seed := range seedData.Users {
		var record models.User
		if res := db.Where(models.User{Model: models.Model{ID: seed.ID}}).FirstOrCreate(&record).Save(&seed); res.Error != nil {
			log.Fatalf("error: seeding user failed: %s", res.Error)
		}
	}
}

// seedAuthenticationProviders synchronizes authentication provider information from the seed data.
func seedAuthenticationProviders(db *gorm.DB, seedData *SeedData) {
	// Ensure that all records exist and have the desired information.
	for _, seed := range seedData.AuthenticationProviders {
		var record models.AuthenticationProvider
		if res := db.Where(models.AuthenticationProvider{Model: models.Model{ID: seed.ID}}).FirstOrCreate(&record).Save(&seed); res.Error != nil {
			log.Fatalf("error: seeding authentication provider failed: %s", res.Error)
		}
	}
}
