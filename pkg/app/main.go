package app

import (
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	chiMiddleware "github.com/go-chi/chi/middleware"
	"github.com/go-chi/jwtauth"

	"gitlab.com/intric-dk/webservices/api/pkg/handlers"
	"gitlab.com/intric-dk/webservices/api/pkg/middleware"
	"gitlab.com/intric-dk/webservices/api/pkg/migration"
	"gitlab.com/intric-dk/webservices/api/pkg/routers"
	"gitlab.com/intric-dk/webservices/api/pkg/util"
)

// Serve runs the REST API HTTP server.
func Serve() {
	// Print version number.
	log.Printf("Application version: %s\n", util.GetVersion())

	// Get environment variables.
	dbURL := util.Getenv("DB_URL")
	if dbURL == "" {
		log.Fatalf("error: environment variable may not be empty: DB_URL")
	}
	jwtSecret := util.Getenv("JWT_SECRET")
	if jwtSecret == "" {
		log.Fatalf("error: environment variable may not be empty: JWT_SECRET")
	}
	port := util.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	loglevel := util.Getenv("LOGLEVEL")
	httpLoglevel := 200
	if loglevel != "" {
		var err error
		httpLoglevel, err = strconv.Atoi(loglevel)
		if err != nil || httpLoglevel >= 600 {
			log.Fatalf("error: environment variable invalid: LOGLEVEL")
		}
	}
	seed := util.Getenv("SEED")

	// Configure JWT authentication.
	tokenAuth := jwtauth.New("HS512", []byte(jwtSecret), nil)

	// Connect to database.
	db := util.ConnectDB(dbURL)

	// Configure casbin authorization.
	util.InitializeEnforcer(db)

	// Migrate database schemas.
	migration.DB(db)

	// Seed database with important data.
	migration.Seed(db, seed)

	// Define content type.
	contentType := "application/json"

	// Create router and configure endpoints.
	router := chi.NewRouter()
	router.Use(middleware.CORS())
	router.Use(chiMiddleware.RedirectSlashes)
	router.Use(chiMiddleware.RequestID)
	router.Use(chiMiddleware.RealIP)
	router.Use(chiMiddleware.SetHeader("Content-Type", contentType))
	router.Use(chiMiddleware.Compress(5, contentType))
	router.Use(jwtauth.Verify(tokenAuth, jwtauth.TokenFromHeader))
	router.Use(middleware.AuthN())
	router.Use(middleware.Logger(httpLoglevel))
	router.Use(middleware.DB(db))
	router.Mount("/v1/me", routers.Me())
	router.Mount("/v1/organisations", routers.Organisation())
	router.Mount("/v1/projects", routers.Project())
	router.Mount("/v1/authentication_providers", routers.AuthenticationProvider())
	router.Mount("/v1/users", routers.User())
	router.Mount("/v1/health", routers.Health())
	router.NotFound(handlers.NotFound())

	// Create HTTP server.
	server := &http.Server{Addr: ":" + port,
		Handler:      router,
		ReadTimeout:  2 * time.Second,
		WriteTimeout: 2 * time.Second,
	}

	// Start HTTP server.
	log.Println("Server online: http://localhost:" + port)
	log.Fatalf("error: server crashed: %s", server.ListenAndServe())
}
