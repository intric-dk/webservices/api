package middleware

import (
	"log"
	"net/http"
	"time"

	chiMiddleware "github.com/go-chi/chi/middleware"
)

// Logger is a middleware that logs response codes, paths and times.
func Logger(httpLoglevel int) func(next http.Handler) http.Handler {
	log.Printf("Using loglevel: %d", httpLoglevel)

	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			// Get wrapped response writer and request start.
			ww := chiMiddleware.NewWrapResponseWriter(w, r.ProtoMajor)
			start := time.Now()

			// Execute function once the request finishes.
			defer func() {
				if ww.Status() >= httpLoglevel {
					log.Printf("%s %s %d %dB %s\n", r.Method, r.URL.Path, ww.Status(), ww.BytesWritten(), time.Since(start))
				}
			}()

			// Serve middlewares.
			next.ServeHTTP(ww, r)
		}

		// Create handler function.
		return http.HandlerFunc(fn)
	}
}
