package middleware

import (
	"context"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/jwtauth"
	"gitlab.com/intric-dk/webservices/api/pkg/util"
)

// AuthNInfo stores information about the authenticated entity,
// which can be either a user or a service.
type AuthNInfo struct {
	UserID         string
	OrganisationID string
}

const (
	// ContextKeyAuthN is the key to access the authentication information in the context.
	ContextKeyAuthN = util.ContextKey("authn")
	// AuthNAnonymousUserID is the value of the user ID if no credentials were provided.
	AuthNAnonymousUserID = "anonymous"
	// AuthNAnonymousOrganisationID is the value of organisation ID if no credentials were provided.
	AuthNAnonymousOrganisationID = "global"
)

// AuthN is a middleware that extracts the user information from the JWT.
func AuthN() func(next http.Handler) http.Handler {
	// Return a new middleware function.
	return func(next http.Handler) http.Handler {
		// Middleware returns an HTTP handler.
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// Read claims and token from context.
			token, claims, err := jwtauth.FromContext(r.Context())

			// Check if any error occured.
			if err != nil {
				// Check if the middleware library detected an error.
				switch err {
				case jwtauth.ErrExpired:
					util.SendError(w, http.StatusUnauthorized, util.ReasonTokenExpired)
					return
				case jwtauth.ErrIATInvalid:
					util.SendError(w, http.StatusUnauthorized, util.ReasonIATClaimInvalid)
					return
				case jwtauth.ErrNBFInvalid:
					util.SendError(w, http.StatusUnauthorized, util.ReasonNBFClaimInvalid)
					return
				case jwtauth.ErrAlgoInvalid:
					util.SendError(w, http.StatusUnauthorized, util.ReasonAlgorithmInvalid)
					return
				}

				// Check if the JWT library detected an error.
				if verr, ok := err.(*jwt.ValidationError); ok {
					// Check for well-known error flags.
					switch {
					case verr.Errors&jwt.ValidationErrorSignatureInvalid > 0:
						util.SendError(w, http.StatusUnauthorized, util.ReasonSignatureInvalid)
						return
					}
				}
			}

			// Check if the token is valid and the claims are valid.
			if token != nil && token.Valid && claims != nil {
				// Get user ID claim.
				userID, userIDOk := claims["user_id"].(string)
				if !userIDOk || len(userID) != 36 {
					util.SendError(w, http.StatusUnauthorized, util.ReasonUserIDClaimInvalid)
					return
				}

				// Get organisation ID claim.
				organisationID, organisationIDOk := claims["organisation_id"].(string)
				if !organisationIDOk || (len(organisationID) != 36 && organisationID != "guest") {
					util.SendError(w, http.StatusUnauthorized, util.ReasonOrganisationIDClaimInvalid)
					return
				}

				// Inject authentication info into request context.
				ctx := context.WithValue(r.Context(), ContextKeyAuthN, &AuthNInfo{
					UserID:         userID,
					OrganisationID: organisationID,
				})

				// Call next middleware.
				next.ServeHTTP(w, r.WithContext(ctx))
				return
			}

			// User is anonymous if no token was provided.
			ctx := context.WithValue(r.Context(), ContextKeyAuthN, &AuthNInfo{
				UserID:         AuthNAnonymousUserID,
				OrganisationID: AuthNAnonymousOrganisationID,
			})

			// Call next middleware.
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
