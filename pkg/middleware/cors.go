package middleware

import (
	"net/http"

	"github.com/go-chi/cors"
)

// CORS configures the cross-origin resource sharing policies.
// Required for most REST APIs to be accessed from within the
// browser via for example the native Fetch API.
func CORS() func(next http.Handler) http.Handler {
	// Configure CORS settings.
	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           600,
	})

	return cors.Handler
}
