package middleware

import (
	"context"
	"net/http"
	"reflect"
	"strings"

	"gitlab.com/intric-dk/webservices/api/pkg/util"
)

const (
	// ContextKeyAuthZ is the key to access the authorization information in the context, which indicates field-level access.
	ContextKeyAuthZ = util.ContextKey("authz")
)

// AuthZRequest defines an authorization request.
type AuthZRequest struct {
	// The type of access as a CRUD verb.
	Verb string
	// The requested HTTP resource as a generic endpoint.
	Resource string
}

// NewAuthZRequest creates a new authorization request by parsing the URL path and URL method.
func NewAuthZRequest(httpMethod string, httpPath string) AuthZRequest {
	// Create new authorization request.
	return AuthZRequest{
		Verb:     httpMethod,
		Resource: "rest://" + httpPath,
	}
}

// AuthZ is a middleware that verifies a user and its roles against access control policies.
func AuthZ(model interface{}) func(next http.Handler) http.Handler {
	// Return a new middleware function.
	return func(next http.Handler) http.Handler {
		// Middleware returns an HTTP handler.
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// Get current authorization request.
			ar := NewAuthZRequest(r.Method, r.URL.Path)

			// Get request context.
			ctx := r.Context()

			// Get authentication information from context.
			authNInfo := r.Context().Value(ContextKeyAuthN).(*AuthNInfo)

			// Gather accessible fields.
			fields := []string{"id", "created_at", "updated_at"}
			defaultFieldCount := len(fields)

			// Check which fields can be accessed.
			for _, field := range fieldNames(model) {
				canAccessField, err := util.Enforcer().Enforce(authNInfo.UserID, authNInfo.OrganisationID, ar.Verb, ar.Resource, field)
				if err != nil {
					util.SendError(w, http.StatusForbidden, util.ReasonPolicyCheckFailed)
					return
				}
				if canAccessField {
					fields = append(fields, field)
				}
			}

			// Check if user has any access.
			if len(fields) == defaultFieldCount {
				util.SendError(w, http.StatusForbidden, util.ReasonInsufficientPrivileges)
				return
			}

			// Inject accessible fields into context.
			ctx = context.WithValue(ctx, ContextKeyAuthZ, fields)

			// Call next middleware.
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

// fieldNames returns the available fields of a database model struct.
func fieldNames(model interface{}) []string {
	// Get type of struct via runtime reflection.
	structType := reflect.TypeOf(model)

	// Get field names from JSON tags.
	var fields []string
	for i := 0; i < structType.NumField(); i++ {
		field := strings.Split(structType.Field(i).Tag.Get("json"), ",")[0]
		if field != "" && field != "-" {
			fields = append(fields, field)
		}
	}

	return fields
}
