package middleware

import (
	"context"
	"net/http"

	"github.com/jinzhu/gorm"

	"gitlab.com/intric-dk/webservices/api/pkg/util"
)

const (
	// ContextKeyDB is the key to access the database in the context.
	ContextKeyDB = util.ContextKey("db")
)

// DB is a middleware that exposes the database as part of the request context.
func DB(db *gorm.DB) func(next http.Handler) http.Handler {
	// Return a new middleware function.
	return func(next http.Handler) http.Handler {
		// Middleware returns an HTTP handler.
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// Inject database into request context.
			ctx := context.WithValue(r.Context(), ContextKeyDB, db)

			// Call next middleware.
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
