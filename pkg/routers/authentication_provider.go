package routers

import (
	"encoding/json"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/chi"
	"github.com/go-playground/validator"
	"github.com/jinzhu/gorm"
	"github.com/markbates/goth/gothic"

	"gitlab.com/intric-dk/webservices/api/pkg/middleware"
	"gitlab.com/intric-dk/webservices/api/pkg/models"
	"gitlab.com/intric-dk/webservices/api/pkg/util"
)

// AuthenticationProvider returns a router for the service.
func AuthenticationProvider() chi.Router {
	// Initialize external URL to generate correct callback URLs.
	util.InitExternalURL()
	// Initialize the allowed redirect URI.
	util.InitRedirectURI()
	// Initialize the signing secret.
	util.InitJWTSecret()

	// Configure session cookies for authentication library.
	gothic.Store = util.SessionStore()

	// Create a new router.
	router := chi.NewRouter()

	// Configure authorization middleware.
	router.Use(middleware.AuthZ(models.AuthenticationProvider{}))

	// Get all entities.
	router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		// Get request context.
		ctx := r.Context()

		// Get database.
		db := ctx.Value(middleware.ContextKeyDB).(*gorm.DB)

		// Get allowed fields.
		fields := ctx.Value(middleware.ContextKeyAuthZ).([]string)

		// Load entities.
		var entities []models.AuthenticationProvider
		if res := db.Select(fields).Order("created_at").Find(&entities); res.Error != nil {
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		// Send response.
		util.SendData(w, http.StatusOK, entities)
		return
	})

	// Create an entity.
	router.Post("/", func(w http.ResponseWriter, r *http.Request) {
		// Get database.
		db := r.Context().Value(middleware.ContextKeyDB).(*gorm.DB)

		// Parse request payload.
		payload := models.AuthenticationProviderPayload{}

		// Decode request body.
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			util.SendError(w, http.StatusBadRequest, util.ReasonJSONInvalid)
			return
		}

		// Check struct.
		validate := validator.New()
		if err := validate.Struct(&payload); err != nil {
			util.SendError(w, http.StatusBadRequest, util.ReasonValidationFailed)
			return
		}

		// Create new entity.
		entity := models.AuthenticationProvider{
			Name:         payload.Name,
			Slug:         payload.Slug,
			ClientKey:    payload.ClientKey,
			ClientSecret: payload.ClientSecret,
			Provider:     payload.Provider,
			Global:       payload.Global,
		}

		// Create entity.
		if err := db.Create(&entity).Error; err != nil {
			// Check if an entity already exists.
			if strings.Contains(strings.ToLower(err.Error()), "duplicate") {
				util.SendError(w, http.StatusConflict, util.ReasonEntityExists)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		util.SendData(w, http.StatusCreated, entity)
		return
	})

	// Read an entity.
	router.Get("/{slugOrID:[a-z0-9-]+}", func(w http.ResponseWriter, r *http.Request) {
		// Get the entity slug.
		slugOrID := chi.URLParam(r, "slugOrID")

		// Get request context.
		ctx := r.Context()

		// Get database.
		db := ctx.Value(middleware.ContextKeyDB).(*gorm.DB)

		// Get allowed fields.
		fields := ctx.Value(middleware.ContextKeyAuthZ).([]string)

		// Find entity.
		var entity models.AuthenticationProvider
		if res := db.Select(fields).Where("slug = ?", slugOrID).Or("id = ?", slugOrID).First(&entity); res.Error != nil {
			// Check if any entity was found.
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		util.SendData(w, http.StatusOK, entity)
		return
	})

	// Update an entity.
	router.Put("/{slugOrID:[a-z0-9-]+}", func(w http.ResponseWriter, r *http.Request) {
		// Get the entity slug.
		slugOrID := chi.URLParam(r, "slugOrID")

		// Get database.
		db := r.Context().Value(middleware.ContextKeyDB).(*gorm.DB)

		// Parse request payload.
		payload := models.AuthenticationProviderPayload{}

		// Decode request body.
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			util.SendError(w, http.StatusBadRequest, util.ReasonJSONInvalid)
			return
		}

		// Check struct.
		validate := validator.New()
		if err := validate.Struct(&payload); err != nil {
			util.SendError(w, http.StatusBadRequest, util.ReasonValidationFailed)
			return
		}

		// Find entity.
		var entity models.AuthenticationProvider
		if res := db.Where("slug = ?", slugOrID).Or("id = ?", slugOrID).First(&entity); res.Error != nil {
			// Check if any entity was found.
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		// Update entity.
		entity.Name = payload.Name
		entity.Slug = payload.Slug
		entity.ClientKey = payload.ClientKey
		entity.ClientSecret = payload.ClientSecret
		entity.Provider = payload.Provider
		entity.Global = payload.Global

		// Update entity.
		if res := db.Save(&entity); res.Error != nil {
			// Check if any entity was found.
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			// Check if an entity already exists.
			if strings.Contains(strings.ToLower(res.Error.Error()), "duplicate") {
				util.SendError(w, http.StatusConflict, util.ReasonEntityExists)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		util.SendData(w, http.StatusOK, entity)
		return
	})

	// Delete an entity.
	router.Delete("/{slugOrID:[a-z0-9-]+}", func(w http.ResponseWriter, r *http.Request) {
		// Get the entity slug.
		slugOrID := chi.URLParam(r, "slugOrID")

		// Get database.
		db := r.Context().Value(middleware.ContextKeyDB).(*gorm.DB)

		// Find entity.
		var entity models.AuthenticationProvider
		if res := db.Where("slug = ?", slugOrID).Or("id = ?", slugOrID).First(&entity); res.Error != nil {
			// Check if any entity was found.
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		// Delete entity.
		if res := db.Unscoped().Delete(&entity); res.Error != nil {
			// Check if any entity was found.
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		util.SendData(w, http.StatusNoContent, nil)
		return
	})

	router.Get("/{slugOrID:[a-z0-9-]+}/redirects/login", func(w http.ResponseWriter, r *http.Request) {
		// Get the entity slug.
		slugOrID := chi.URLParam(r, "slugOrID")

		// Get database.
		db := r.Context().Value(middleware.ContextKeyDB).(*gorm.DB)

		// Find entity.
		var entity models.AuthenticationProvider
		if res := db.Where("slug = ?", slugOrID).Or("id = ?", slugOrID).First(&entity); res.Error != nil {
			// Check if any entity was found.
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		// Verify redirect URI.
		redirectURI := r.URL.Query().Get("redirect_uri")
		if redirectURI != util.RedirectURI {
			util.SendError(w, http.StatusBadRequest, util.ReasonRedirectURIInvalid)
			return
		}

		// Verify organisation.
		organisationID := r.URL.Query().Get("organisation_id")
		if organisationID == "" {
			util.SendError(w, http.StatusBadRequest, util.ReasonOrganisationIDMissing)
			return
		}
		if organisationID != "guest" {
			var organisation models.Organisation
			if res := db.Where("id = ?", organisationID).First(&organisation); res.Error != nil {
				// Check if any entity was found.
				if res.RecordNotFound() {
					util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
					return
				}

				util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
				return
			}
		}

		// Load authentication provider.
		if err := util.LoadAuthenticationProvider(&entity); err != nil {
			util.SendError(w, http.StatusBadRequest, util.ReasonProviderUnknown)
			return
		}

		// Inject provider into query string.
		util.InjectProvider(r, entity.ID)

		// Inject state into query string.
		util.InjectAuthenticationTransaction(r)

		// Try to get the user without re-authenticating.
		if user, err := gothic.CompleteUserAuth(w, r); err == nil {
			util.SendData(w, http.StatusOK, user)
		} else {
			gothic.BeginAuthHandler(w, r)
		}
	})

	router.Get("/{slugOrID:[a-z0-9-]+}/redirects/authenticate", func(w http.ResponseWriter, r *http.Request) {
		// Get the entity slug.
		slugOrID := chi.URLParam(r, "slugOrID")

		// Get database.
		db := r.Context().Value(middleware.ContextKeyDB).(*gorm.DB)

		// Find entity.
		var entity models.AuthenticationProvider
		if res := db.Where("slug = ?", slugOrID).Or("id = ?", slugOrID).First(&entity); res.Error != nil {
			// Check if any entity was found.
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		// Load authentication provider.
		if err := util.LoadAuthenticationProvider(&entity); err != nil {
			util.SendError(w, http.StatusBadRequest, util.ReasonProviderUnknown)
			return
		}

		// Inject provider into query string.
		util.InjectProvider(r, entity.ID)

		// Get authentication transaction.
		authTx := util.ExtractAuthenticationTransaction(r)
		if authTx == nil {
			util.SendError(w, http.StatusBadRequest, util.ReasonStateInvalid)
			return
		}

		// Load user from session.
		gothUser, err := gothic.CompleteUserAuth(w, r)
		if err != nil {
			util.SendError(w, http.StatusBadRequest, util.ReasonSessionMissing)
			return
		}

		// Find existing user in database.
		newUser := util.UserFromGoth(&gothUser)
		var existingUser models.User
		if res := db.Where(models.User{Email: newUser.Email}).FirstOrCreate(&existingUser); res.Error != nil {
			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		// Update user info.
		existingUser.FirstName = newUser.FirstName
		existingUser.LastName = newUser.LastName
		existingUser.AvatarURL = newUser.AvatarURL

		// Update user info in database.
		if res := db.Save(&existingUser); res.Error != nil {
			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		// Create JWT.
		tokenString, err := jwt.NewWithClaims(jwt.SigningMethodHS512, jwt.MapClaims{
			"user_id":         existingUser.ID,
			"organisation_id": authTx.OrganisationID,
			"exp":             time.Now().Add(30 * 86400 * time.Second).Unix(),
		}).SignedString([]byte(util.JWTSecret))
		if err != nil {
			util.SendError(w, http.StatusInternalServerError, util.ReasonIssuingJWTFailed)
			return
		}

		// Create full redirect URI.
		redirect, err := url.Parse(authTx.RedirectURI)
		if err != nil {
			util.SendError(w, http.StatusInternalServerError, util.ReasonRedirectURIInvalid)
			return
		}
		redirect.Fragment = "organisation_id=" + authTx.OrganisationID + "&bearer=" + tokenString

		// Redirect user to frontend.
		http.Redirect(w, r, redirect.String(), http.StatusTemporaryRedirect)
	})

	// Return created router.
	return router
}
