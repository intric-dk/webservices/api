package routers

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	"github.com/stretchr/testify/assert"
	"gitlab.com/intric-dk/webservices/api/pkg/util"
)

func TestHealthReadMany(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	url := "/v1/health"
	req, err := http.NewRequest("GET", url, nil)
	assert.Nil(err)

	// We create a response recorder to record the response.
	rr := httptest.NewRecorder()

	// Serve via response recorder.
	router := chi.NewRouter()
	router.Mount(url, Health())
	router.ServeHTTP(rr, req)

	// Check the status code.
	assert.Equal(http.StatusOK, rr.Code)

	// Decode response body.
	var body util.DataPayload
	err = json.NewDecoder(rr.Body).Decode(&body)
	assert.Nil(err)
	bodyData := body.Data.(map[string]interface{})
	hostname := bodyData["hostname"].(string)
	startedAt := bodyData["started_at"].(string)
	version := bodyData["version"].(string)

	// Assert.
	assert.Equal(3, len(bodyData))
	assert.Equal(health.Hostname, hostname)
	assert.Equal(health.StartedAt, startedAt)
	assert.Equal(health.Version, version)
}
