package routers

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/go-chi/chi"

	"gitlab.com/intric-dk/webservices/api/pkg/util"
)

// health holds the cached reponse for the application server health.
var health HealthData

// HealthData provides information about the application server health.
type HealthData struct {
	Hostname  string `json:"hostname"`
	Version   string `json:"version"`
	StartedAt string `json:"started_at"`
}

// Health returns a router for the service.
func Health() chi.Router {
	// Get application server information.
	version := util.GetVersion()
	startedAt := time.Now().UTC().Format(time.RFC3339)
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatalf("error: reading hostname failed: %s", err)
	}

	// Cache application health.
	health = HealthData{
		hostname,
		version,
		startedAt,
	}

	// Create a new router.
	router := chi.NewRouter()

	// Read information about the current application server status.
	router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		json.NewEncoder(w).Encode(&util.DataPayload{
			Data: health,
		})
	})

	// Return created router.
	return router
}
