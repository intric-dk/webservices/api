package routers

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/go-chi/chi"
	"github.com/go-playground/validator"
	"github.com/jinzhu/gorm"

	"gitlab.com/intric-dk/webservices/api/pkg/middleware"
	"gitlab.com/intric-dk/webservices/api/pkg/models"
	"gitlab.com/intric-dk/webservices/api/pkg/util"
)

// User returns a router for the service.
func User() chi.Router {
	// Create a new router.
	router := chi.NewRouter()

	// Configure authorization middleware.
	router.Use(middleware.AuthZ(models.User{}))

	// Get all entities.
	router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		// Get database.
		db := r.Context().Value(middleware.ContextKeyDB).(*gorm.DB)

		// Load entities.
		var entities []models.User
		if res := db.Order("created_at").Find(&entities); res.Error != nil {
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		// Send response.
		util.SendData(w, http.StatusOK, entities)
		return
	})

	// Create an entity.
	router.Post("/", func(w http.ResponseWriter, r *http.Request) {
		// Get database.
		db := r.Context().Value(middleware.ContextKeyDB).(*gorm.DB)

		// Parse request payload.
		payload := models.UserPayload{}

		// Decode request body.
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			util.SendError(w, http.StatusBadRequest, util.ReasonJSONInvalid)
			return
		}

		// Check struct.
		validate := validator.New()
		if err := validate.Struct(&payload); err != nil {
			util.SendError(w, http.StatusBadRequest, util.ReasonValidationFailed)
			return
		}

		// Create new entity.
		entity := models.User{
			FirstName: payload.FirstName,
			LastName:  payload.LastName,
			Email:     payload.Email,
			AvatarURL: payload.AvatarURL,
		}

		// Create entity.
		if err := db.Create(&entity).Error; err != nil {
			// Check if an entity already exists.
			if strings.Contains(strings.ToLower(err.Error()), "duplicate") {
				util.SendError(w, http.StatusConflict, util.ReasonEntityExists)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		util.SendData(w, http.StatusCreated, entity)
		return
	})

	// Read an entity.
	router.Get("/{id:[a-z0-9-]+}", func(w http.ResponseWriter, r *http.Request) {
		// Get the entity slug.
		id := chi.URLParam(r, "id")

		// Get database.
		db := r.Context().Value(middleware.ContextKeyDB).(*gorm.DB)

		// Find entity.
		var entity models.User
		if res := db.Where("id = ?", id).First(&entity); res.Error != nil {
			// Check if any entity was found.
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		util.SendData(w, http.StatusOK, entity)
		return
	})

	// Update an entity.
	router.Put("/{id:[a-z0-9-]+}", func(w http.ResponseWriter, r *http.Request) {
		// Get the entity slug.
		id := chi.URLParam(r, "id")

		// Get database.
		db := r.Context().Value(middleware.ContextKeyDB).(*gorm.DB)

		// Parse request payload.
		payload := models.UserPayload{}

		// Decode request body.
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			util.SendError(w, http.StatusBadRequest, util.ReasonJSONInvalid)
			return
		}

		// Check struct.
		validate := validator.New()
		if err := validate.Struct(&payload); err != nil {
			util.SendError(w, http.StatusBadRequest, util.ReasonValidationFailed)
			return
		}

		// Find entity.
		var entity models.User
		if res := db.Where("id = ?", id).First(&entity); res.Error != nil {
			// Check if any entity was found.
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		// Update entity.
		entity.FirstName = payload.FirstName
		entity.LastName = payload.LastName
		entity.Email = payload.Email
		entity.AvatarURL = payload.AvatarURL

		// Update entity.
		if res := db.Save(&entity); res.Error != nil {
			// Check if any entity was found.
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			// Check if an entity already exists.
			if strings.Contains(strings.ToLower(res.Error.Error()), "duplicate") {
				util.SendError(w, http.StatusConflict, util.ReasonEntityExists)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		util.SendData(w, http.StatusOK, entity)
		return
	})

	// Delete an entity.
	router.Delete("/{id:[a-z0-9-]+}", func(w http.ResponseWriter, r *http.Request) {
		// Get the entity slug.
		id := chi.URLParam(r, "id")

		// Get database.
		db := r.Context().Value(middleware.ContextKeyDB).(*gorm.DB)

		// Find entity.
		var entity models.User
		if res := db.Where("id = ?", id).First(&entity); res.Error != nil {
			// Check if any entity was found.
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		// Delete entity.
		if res := db.Unscoped().Delete(&entity); res.Error != nil {
			// Check if any entity was found.
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		util.SendData(w, http.StatusNoContent, nil)
		return
	})

	// Return created router.
	return router
}
