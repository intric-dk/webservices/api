package routers

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"

	"github.com/go-chi/chi"
	"github.com/go-playground/validator"
	"github.com/jinzhu/gorm"

	"gitlab.com/intric-dk/webservices/api/pkg/middleware"
	"gitlab.com/intric-dk/webservices/api/pkg/models"
	"gitlab.com/intric-dk/webservices/api/pkg/util"
)

// Organisation returns a router for the service.
func Organisation() chi.Router {
	// Create a new router.
	router := chi.NewRouter()

	// Configure authorization middleware.
	router.Use(middleware.AuthZ(models.Organisation{}))

	// Get all entities.
	router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		// Get database.
		db := r.Context().Value(middleware.ContextKeyDB).(*gorm.DB)

		// Get search parameter.
		search := strings.ToLower(r.URL.Query().Get("q"))
		if search != "" {
			db = db.Where("lower(name) LIKE ?", "%"+search+"%")
		}

		// Load entities.
		var entities []models.Organisation
		if res := db.Order("created_at").Find(&entities); res.Error != nil {
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			log.Println(res.Error)

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		// Send response.
		util.SendData(w, http.StatusOK, entities)
		return
	})

	// Create an entity.
	router.Post("/", func(w http.ResponseWriter, r *http.Request) {
		// Get database.
		db := r.Context().Value(middleware.ContextKeyDB).(*gorm.DB)

		// Parse request payload.
		payload := models.OrganisationPayload{}

		// Decode request body.
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			util.SendError(w, http.StatusBadRequest, util.ReasonJSONInvalid)
			return
		}

		// Check struct.
		validate := validator.New()
		if err := validate.Struct(&payload); err != nil {
			util.SendError(w, http.StatusBadRequest, util.ReasonValidationFailed)
			return
		}

		// Create new entity.
		entity := models.Organisation{
			Name:        payload.Name,
			Slug:        payload.Slug,
			Description: payload.Description,
			AvatarURL:   payload.AvatarURL,
		}

		// Create entity.
		if err := db.Create(&entity).Error; err != nil {
			// Check if an entity already exists.
			if strings.Contains(strings.ToLower(err.Error()), "duplicate") {
				util.SendError(w, http.StatusConflict, util.ReasonEntityExists)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		util.SendData(w, http.StatusCreated, entity)
		return
	})

	// Read an entity.
	router.Get("/{slugOrID:[a-z0-9-]+}", func(w http.ResponseWriter, r *http.Request) {
		// Get the entity slug.
		slugOrID := chi.URLParam(r, "slugOrID")

		// Get database.
		db := r.Context().Value(middleware.ContextKeyDB).(*gorm.DB)

		// Find entity.
		var entity models.Organisation
		if res := db.Where("slug = ?", slugOrID).Or("id = ?", slugOrID).First(&entity); res.Error != nil {
			// Check if any entity was found.
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		util.SendData(w, http.StatusOK, entity)
		return
	})

	// Update an entity.
	router.Put("/{slugOrID:[a-z0-9-]+}", func(w http.ResponseWriter, r *http.Request) {
		// Get the entity slug.
		slugOrID := chi.URLParam(r, "slugOrID")

		// Get database.
		db := r.Context().Value(middleware.ContextKeyDB).(*gorm.DB)

		// Parse request payload.
		payload := models.OrganisationPayload{}

		// Decode request body.
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			util.SendError(w, http.StatusBadRequest, util.ReasonJSONInvalid)
			return
		}

		// Check struct.
		validate := validator.New()
		if err := validate.Struct(&payload); err != nil {
			util.SendError(w, http.StatusBadRequest, util.ReasonValidationFailed)
			return
		}

		// Find entity.
		var entity models.Organisation
		if res := db.Where("slug = ?", slugOrID).Or("id = ?", slugOrID).First(&entity); res.Error != nil {
			// Check if any entity was found.
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		// Update entity.
		entity.Name = payload.Name
		entity.Slug = payload.Slug
		entity.Description = payload.Description
		entity.AvatarURL = payload.AvatarURL

		// Update entity.
		if res := db.Save(&entity); res.Error != nil {
			// Check if any entity was found.
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			// Check if an entity already exists.
			if strings.Contains(strings.ToLower(res.Error.Error()), "duplicate") {
				util.SendError(w, http.StatusConflict, util.ReasonEntityExists)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		util.SendData(w, http.StatusOK, entity)
		return
	})

	// Delete an entity.
	router.Delete("/{slugOrID:[a-z0-9-]+}", func(w http.ResponseWriter, r *http.Request) {
		// Get the entity slug.
		slugOrID := chi.URLParam(r, "slugOrID")

		// Get database.
		db := r.Context().Value(middleware.ContextKeyDB).(*gorm.DB)

		// Find entity.
		var entity models.Organisation
		if res := db.Where("slug = ?", slugOrID).Or("id = ?", slugOrID).First(&entity); res.Error != nil {
			// Check if any entity was found.
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		// Delete entity.
		if res := db.Unscoped().Delete(&entity); res.Error != nil {
			// Check if any entity was found.
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		util.SendData(w, http.StatusNoContent, nil)
		return
	})

	router.Get("/{slugOrID:[a-z0-9-]+}/projects", func(w http.ResponseWriter, r *http.Request) {
		// Get the entity slug.
		id := chi.URLParam(r, "slugOrID")

		// Get database.
		db := r.Context().Value(middleware.ContextKeyDB).(*gorm.DB)

		// Get search parameter.
		search := strings.ToLower(r.URL.Query().Get("q"))
		if search != "" {
			db = db.Where("lower(name) LIKE ?", "%"+search+"%")
		}

		// Load entities.
		var entities []models.Project
		if res := db.Where("organisation_id = ?", id).Order("created_at").Find(&entities); res.Error != nil {
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		// Send response.
		util.SendData(w, http.StatusOK, entities)
		return
	})

	// Return created router.
	return router
}
