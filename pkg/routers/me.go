package routers

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/jinzhu/gorm"

	"gitlab.com/intric-dk/webservices/api/pkg/middleware"
	"gitlab.com/intric-dk/webservices/api/pkg/models"
	"gitlab.com/intric-dk/webservices/api/pkg/util"
)

// Me returns a router for the service.
func Me() chi.Router {
	// Create a new router.
	router := chi.NewRouter()

	// Read an entity.
	router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		// Get request context.
		ctx := r.Context()

		// Get database.
		db := ctx.Value(middleware.ContextKeyDB).(*gorm.DB)

		// Get authentication information from context.
		authNInfo := ctx.Value(middleware.ContextKeyAuthN).(*middleware.AuthNInfo)

		// Find entity.
		var entity models.User
		if res := db.Where("id = ?", authNInfo.UserID).First(&entity); res.Error != nil {
			// Check if any entity was found.
			if res.RecordNotFound() {
				util.SendError(w, http.StatusNotFound, util.ReasonEntityUnknown)
				return
			}

			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}

		util.SendData(w, http.StatusOK, entity)
		return
	})

	// Return created router.
	return router
}
