package handlers

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/intric-dk/webservices/api/pkg/util"
)

func TestNotFound(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	url := "/not_found"
	req, err := http.NewRequest("GET", url, nil)
	assert.Nil(err)

	// We create a response recorder to record the response.
	rr := httptest.NewRecorder()

	// Serve via response recorder.
	handler := http.HandlerFunc(NotFound())
	handler.ServeHTTP(rr, req)

	// Check the status code.
	assert.Equal(http.StatusNotFound, rr.Code)

	// Decode response body.
	var body util.ErrorPayload
	err = json.NewDecoder(rr.Body).Decode(&body)
	assert.Nil(err)
	bodyError := body.Error.(map[string]interface{})
	title := bodyError["title"].(string)
	status := int(bodyError["status"].(float64))
	reason := bodyError["reason"].(string)

	// Assert.
	assert.Equal(3, len(bodyError))
	assert.Equal(http.StatusText(http.StatusNotFound), title)
	assert.Equal(http.StatusNotFound, status)
	assert.Equal(util.ReasonEndpointUnsupported, reason)
}
