package handlers

import (
	"net/http"

	"gitlab.com/intric-dk/webservices/api/pkg/util"
)

// NotFound returns a handler for endpoints that do not exist.
func NotFound() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		util.SendError(w, http.StatusNotFound, util.ReasonEndpointUnsupported)
	}
}
