package models

import (
	"html"
	"strings"

	"github.com/jinzhu/gorm"
)

// UserPayload is the data that is accessible via the request body.
type UserPayload struct {
	FirstName string `json:"first_name,omitempty" validate:"required,min=2,max=200" gorm:"size:200"`
	LastName  string `json:"last_name,omitempty" validate:"required,min=2,max=200" gorm:"size:200"`
	Email     string `json:"email,omitempty" validate:"required,max=200,email" gorm:"size:200;unique_index"`
	AvatarURL string `json:"avatar_url,omitempty" validate:"omitempty,max=500,url" gorm:"size:500"`
}

// User is an application user.
type User struct {
	// Inherit properties from base model.
	Model

	FirstName string `json:"first_name,omitempty" validate:"required,min=2,max=200" gorm:"size:200"`
	LastName  string `json:"last_name,omitempty" validate:"required,min=2,max=200" gorm:"size:200"`
	Email     string `json:"email,omitempty" validate:"required,max=200,email" gorm:"size:200;unique_index"`
	AvatarURL string `json:"avatar_url,omitempty" validate:"omitempty,max=500,url" gorm:"size:500"`
}

// BeforeCreate will set a UUID rather than numeric ID.
func (u *User) BeforeCreate(scope *gorm.Scope) error {
	CreateUUID(scope)
	return nil
}

// BeforeSave will sanitize fields to remove potentially dangerous characters.
func (u *User) BeforeSave(scope *gorm.Scope) error {
	scope.SetColumn("FirstName", html.EscapeString(strings.TrimSpace(u.FirstName)))
	scope.SetColumn("LastName", html.EscapeString(strings.TrimSpace(u.LastName)))
	scope.SetColumn("Email", strings.TrimSpace(u.Email))
	return nil
}
