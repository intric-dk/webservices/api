package models

import (
	"html"
	"strings"

	"github.com/gosimple/slug"
	"github.com/jinzhu/gorm"
)

// ProjectPayload is the data that is accessible via the request body.
type ProjectPayload struct {
	Name           string `json:"name,omitempty" validate:"required,min=1,max=200" gorm:"size:200;unique_index"`
	Slug           string `json:"slug,omitempty" validate:"required,min=1,max=200,printascii" gorm:"size:200;unique_index"`
	Description    string `json:"description,omitempty" validate:"omitempty,max=1000" gorm:"size:1000"`
	AvatarURL      string `json:"avatar_url,omitempty" validate:"omitempty,max=500,url" gorm:"size:500"`
	OrganisationID string `json:"organisation_id,omitempty" validate:"required,uuid4"`
}

// Project groups assets within an organisation.
type Project struct {
	// Inherit properties from base model.
	Model

	Name           string `json:"name,omitempty" validate:"required,min=1,max=200" gorm:"size:200;unique_index"`
	Slug           string `json:"slug,omitempty" validate:"required,min=1,max=200,printascii" gorm:"size:200;unique_index"`
	Description    string `json:"description,omitempty" validate:"omitempty,max=1000" gorm:"size:1000"`
	AvatarURL      string `json:"avatar_url,omitempty" validate:"omitempty,max=500,url" gorm:"size:500"`
	OrganisationID string `json:"organisation_id,omitempty" validate:"required,uuid4"`
}

// BeforeCreate will set a UUID rather than numeric ID.
func (p *Project) BeforeCreate(scope *gorm.Scope) error {
	CreateUUID(scope)
	return nil
}

// BeforeSave will sanitize fields to remove potentially dangerous characters.
func (p *Project) BeforeSave(scope *gorm.Scope) error {
	scope.SetColumn("Name", html.EscapeString(strings.TrimSpace(p.Name)))
	scope.SetColumn("Slug", slug.Make(p.Slug))
	scope.SetColumn("Description", html.EscapeString(strings.TrimSpace(p.Description)))
	return nil
}
