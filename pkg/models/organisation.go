package models

import (
	"html"
	"strings"

	"github.com/gosimple/slug"
	"github.com/jinzhu/gorm"
)

// OrganisationPayload is the data that is accessible via the request body.
type OrganisationPayload struct {
	Name        string `json:"name,omitempty" validate:"required,min=1,max=200" gorm:"size:200;unique_index"`
	Slug        string `json:"slug,omitempty" validate:"required,min=1,max=200,printascii,ne=guest,ne=global" gorm:"size:200;unique_index"`
	Description string `json:"description,omitempty" validate:"omitempty,max=1000" gorm:"size:1000"`
	AvatarURL   string `json:"avatar_url,omitempty" validate:"omitempty,max=500,url" gorm:"size:500"`
}

// Organisation can be a company, a group or a legal entity.
type Organisation struct {
	// Inherit properties from base model.
	Model

	Name        string    `json:"name,omitempty" validate:"required,min=1,max=200" gorm:"size:200;unique_index"`
	Slug        string    `json:"slug,omitempty" validate:"required,min=1,max=200,printascii,ne=guest,ne=global" gorm:"size:200;unique_index"`
	Description string    `json:"description,omitempty" validate:"omitempty,max=1000" gorm:"size:1000"`
	AvatarURL   string    `json:"avatar_url,omitempty" validate:"omitempty,max=500,url" gorm:"size:500"`
	Projects    []Project `json:"-"`
}

// BeforeCreate will set a UUID rather than numeric ID.
func (o *Organisation) BeforeCreate(scope *gorm.Scope) error {
	CreateUUID(scope)
	return nil
}

// BeforeSave will sanitize fields to remove potentially dangerous characters.
func (o *Organisation) BeforeSave(scope *gorm.Scope) error {
	scope.SetColumn("Name", html.EscapeString(strings.TrimSpace(o.Name)))
	scope.SetColumn("Slug", slug.Make(o.Slug))
	scope.SetColumn("Description", html.EscapeString(strings.TrimSpace(o.Description)))
	return nil
}
