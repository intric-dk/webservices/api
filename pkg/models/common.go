package models

import (
	"log"
	"time"

	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

// Model is the base model which includes all shared properties.
type Model struct {
	ID        string     `json:"id" validate:"-" gorm:"primary_key"`
	CreatedAt time.Time  `json:"created_at" validate:"-" gorm:"index"`
	UpdatedAt time.Time  `json:"updated_at" validate:"-" gorm:"index"`
	DeletedAt *time.Time `json:"deleted_at,omitempty" validate:"-" gorm:"index"`
}

// CreateUUID creates the UUID of a model.
func CreateUUID(scope *gorm.Scope) {
	// Generate unique ID.
	id, err := uuid.NewRandom()
	if err != nil {
		log.Fatalf("error: generating UUID failed: %s", err)
	}

	// Set ID to UUID.
	if scope.PrimaryKeyValue() == "" {
		scope.SetColumn("ID", id.String())
	}
}
