package models

import (
	"html"
	"strings"

	"github.com/gosimple/slug"
	"github.com/jinzhu/gorm"
)

// AuthenticationProviderPayload is the data that is accessible via the request body.
type AuthenticationProviderPayload struct {
	Name         string `json:"name,omitempty" validate:"required,min=1,max=200" gorm:"size:200;unique_index"`
	Slug         string `json:"slug,omitempty" validate:"required,min=1,max=200,printascii" gorm:"size:200;unique_index"`
	ClientKey    string `json:"client_key,omitempty" validate:"required,min=1,max=200" gorm:"size:200"`
	ClientSecret string `json:"client_secret,omitempty" validate:"required,min=1,max=200" gorm:"size:200"`
	Provider     string `json:"provider,omitempty" validate:"required,max=200,oneof=gitlab google" gorm:"size:200"`
	Global       bool   `json:"global,omitempty"`
}

// AuthenticationProvider represents an OAuth 2.0 or OpenID Connect provider,
// such as Azure AD, GitLab or Google.
type AuthenticationProvider struct {
	// Inherit properties from base model.
	Model

	Name         string `json:"name,omitempty" validate:"required,min=1,max=200" gorm:"size:200;unique_index"`
	Slug         string `json:"slug,omitempty" validate:"required,min=1,max=200,printascii" gorm:"size:200;unique_index"`
	ClientKey    string `json:"client_key,omitempty" validate:"required,min=1,max=200" gorm:"size:200"`
	ClientSecret string `json:"client_secret,omitempty" validate:"required,min=1,max=200" gorm:"size:200"`
	Provider     string `json:"provider,omitempty" validate:"required,max=200,oneof=gitlab google" gorm:"size:200"`
	Global       bool   `json:"global,omitempty"`
}

// BeforeCreate will set a UUID rather than numeric ID.
func (a *AuthenticationProvider) BeforeCreate(scope *gorm.Scope) error {
	CreateUUID(scope)
	return nil
}

// BeforeSave will sanitize fields to remove potentially dangerous characters.
func (a *AuthenticationProvider) BeforeSave(scope *gorm.Scope) error {
	scope.SetColumn("Name", html.EscapeString(strings.TrimSpace(a.Name)))
	scope.SetColumn("ClientKey", strings.TrimSpace(a.ClientKey))
	scope.SetColumn("ClientSecret", strings.TrimSpace(a.ClientSecret))
	scope.SetColumn("Provider", strings.TrimSpace(a.Provider))

	// Enforce single global instance of authentication providers
	// by forcing the slug to be equal to the provider name.
	if a.Global {
		scope.SetColumn("Slug", slug.Make(a.Provider))
	} else {
		scope.SetColumn("Slug", slug.Make(a.Slug))
	}

	return nil
}
