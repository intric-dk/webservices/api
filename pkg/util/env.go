package util

import (
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"strings"

	"github.com/joho/godotenv"
)

// Load environment variables from ".env" file.
func init() {
	// Load ".env" file.
	err := godotenv.Load()
	if err != nil {
		log.Println("No .env file found!")
		log.Println("Using environment variables!")
	}
}

// Getenv attempts to get the value of an environment variable from
// a file path. If the value is not a file path, it will use the value.
func Getenv(variable string) string {
	// Get value of environment variable.
	value := os.Getenv(variable)

	// Attempt to parse value of environment variable as file.
	fileURL, err := url.Parse(value)
	if err != nil || fileURL.Scheme != "file" {
		return value
	}

	// Read value from file.
	bytes, err := ioutil.ReadFile(fileURL.Path)
	if err != nil {
		log.Fatalf("error: %s", err)
	}

	return strings.TrimSpace(string(bytes))
}
