package util

import (
	"log"

	"github.com/casbin/casbin/v2"
	"github.com/casbin/casbin/v2/model"
	gormAdapter "github.com/casbin/gorm-adapter/v2"
	"github.com/jinzhu/gorm"
)

// enforcer is a casbin enforcer for authorization policies.
var enforcer *casbin.Enforcer

// adapter is a casbin storage adapter for GORM.
var adapter *gormAdapter.Adapter

// InitializeEnforcer creates a new enforcer via the GORM database adapter.
func InitializeEnforcer(db *gorm.DB) {
	var err error

	// Create new casbin storage adapter.
	adapter, err = gormAdapter.NewAdapterByDB(db)
	if err != nil {
		log.Fatalf("error: creating casbin adapter failed: %s", err)
	}

	// Create new enforcer.
	enforcer, err = casbin.NewEnforcer(authZModel(), adapter)
	if err != nil {
		log.Fatalf("error: creating casbin enforcer failed: %s", err)
	}
}

// Enforcer returns the enforcer instance of the server.
func Enforcer() *casbin.Enforcer {
	return enforcer
}

// authZModel returns the current casbin authorization model.
func authZModel() model.Model {
	m := model.NewModel()
	m.AddDef("r", "r", "sub, org, vrb, res, fld")
	m.AddDef("p", "p", "sub, org, vrb, res, fld, eft")
	m.AddDef("g", "g", "_, _")
	m.AddDef("g", "g2", "_, _, _")
	m.AddDef("e", "e", "some(where (p.eft == allow)) && !some(where (p.eft == deny))")
	m.AddDef("m", "m", "(g(r.sub, p.sub) || g2(r.sub, p.sub, r.org)) && keyMatch(r.res, p.res) && globMatch(r.fld, p.fld) && globMatch(r.vrb, p.vrb)")
	return m
}
