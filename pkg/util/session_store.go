package util

import (
	"log"

	"github.com/gorilla/sessions"
)

// SessionStore configures and returns a new session store.
func SessionStore() *sessions.CookieStore {
	// Load session secret environment variable.
	sessionSecret := Getenv("SESSION_SECRET")
	if sessionSecret == "" {
		log.Fatalf("error: environment variable may not be empty: SESSION_SECRET")
	}

	// Create a new cookie store for the session.
	store := sessions.NewCookieStore([]byte(sessionSecret))

	// Set expiry time to 30 days.
	store.MaxAge(86400 * 30)

	// Configure miscellaneous settings
	store.Options.Path = "/"
	store.Options.HttpOnly = true
	store.Options.Secure = false

	return store
}
