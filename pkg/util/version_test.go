package util

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSetVersion(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	expectedVersion := "TestSetVersion"

	// Act.
	SetVersion(expectedVersion)

	// Assert.
	assert.Equal(expectedVersion, version)
}

func TestGetVersion(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	expectedVersion := "TestGetVersion"
	version = expectedVersion

	// Act.
	fetchedVersion := GetVersion()

	// Assert.
	assert.Equal(expectedVersion, fetchedVersion)
}
