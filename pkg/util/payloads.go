package util

import (
	"net/http"
)

// HTTPError is the information about an HTTP error.
type HTTPError struct {
	Title  string `json:"title"`
	Status int    `json:"status"`
	Reason string `json:"reason"`
}

// ErrorPayload is the structure that responses will have if an error occurs.
type ErrorPayload struct {
	Error interface{} `json:"error"`
}

// DataPayload is the structure that responses will have if data is sent occurs.
type DataPayload struct {
	Data interface{} `json:"data"`
}

// NewHTTPError creates the payload for a new HTTP error.
func NewHTTPError(status int, reason string) *ErrorPayload {
	// Set fallback reason text.
	if reason == "" {
		reason = "Reason Unknown"
	}

	// Return newly created payload.
	return &ErrorPayload{
		HTTPError{
			http.StatusText(status),
			status,
			reason,
		},
	}
}
