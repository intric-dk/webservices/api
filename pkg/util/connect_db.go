package util

import (
	"fmt"
	"log"
	"net/url"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

// ConnectionInformation contains the parsed database driver and the parse database path.
type ConnectionInformation struct {
	Driver string
	Path   string
}

// ConnectDB connects to a database. It provides a database-agnostic
// interface, so the connection string format is a URL for MySQL,
// MariaDB and PostgreSQL.
func ConnectDB(rawURL string) *gorm.DB {
	// Parse connection URL.
	parsedURL, err := url.Parse(rawURL)
	if err != nil {
		log.Fatalf("error: parsing connection string failed: %s", err)
	}

	// Create connection information object.
	var connInfo ConnectionInformation

	// Ensure that MariaDB also uses the MySQL driver.
	connInfo.Driver = parsedURL.Scheme
	if connInfo.Driver == "mariadb" {
		connInfo.Driver = "mysql"
	}

	// Extract information and set defaults.
	hostname := parsedURL.Hostname()
	if hostname == "" {
		hostname = "localhost"
	}
	port := parsedURL.Port()
	user := parsedURL.User.Username()
	password, _ := parsedURL.User.Password()
	dbName := strings.TrimPrefix(parsedURL.Path, "/")

	// Create DB path for MySQL driver.
	if connInfo.Driver == "mysql" {
		if port == "" {
			port = "3306"
		}
		options := parsedURL.RawQuery
		connInfo.Path = fmt.Sprintf("%s:%s@(%s:%s)/%s?%s", user, password, hostname, port, dbName, options)
	}

	// Create DB path for PostgreSQL driver.
	if connInfo.Driver == "postgres" {
		if port == "" {
			port = "5432"
		}
		options := parsedURL.Query()
		var optionsStrings []string
		for key, value := range options {
			optionsStrings = append(optionsStrings, fmt.Sprintf("%s=%s", key, value[0]))
		}
		optionsString := strings.Join(optionsStrings, " ")
		connInfo.Path = fmt.Sprintf("host=%s port=%s user=%s password=%s database=%s %s", hostname, port, user, password, dbName, optionsString)
	}

	if connInfo.Path != "" {
		// Configure timestamp.
		gorm.NowFunc = func() time.Time {
			return time.Now().UTC()
		}

		// Connect to database.
		db, err := gorm.Open(connInfo.Driver, connInfo.Path)
		if err != nil {
			log.Fatalf("error: connecting to database failed: %s", err)
		}

		// Disable verbose database logs.
		db.LogMode(false)

		log.Printf("Connected to database type: %s", parsedURL.Scheme)
		log.Printf("Using database: %s", strings.TrimPrefix(parsedURL.Path, "/"))
		return db
	}

	// Fail if connection string includes invalid database scheme.
	log.Fatalf("error: unsupported database: %s", parsedURL.Scheme)
	return nil
}
