package util

import (
	"encoding/json"
	"log"
	"net/http"
)

// SendError sends an HTTP error by using the response writer.
func SendError(w http.ResponseWriter, status int, reason string) {
	// Set status code.
	w.WriteHeader(status)

	// Encode response body.
	err := json.NewEncoder(w).Encode(NewHTTPError(status, reason))
	if err != nil {
		log.Fatalf("error: encoding response error failed: %s", err)
	}
}

// SendData sends a data response by using the response writer.
func SendData(w http.ResponseWriter, status int, data interface{}) {
	// Set status code.
	w.WriteHeader(status)

	// Encode response body.
	if data != nil {
		err := json.NewEncoder(w).Encode(&DataPayload{
			Data: data,
		})
		if err != nil {
			log.Fatalf("error: encoding response data failed: %s", err)
		}
	}
}
