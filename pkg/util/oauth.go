package util

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"errors"
	"io"
	"log"
	"net/http"
	"strings"

	"github.com/markbates/goth"
	"github.com/markbates/goth/providers/gitlab"
	"github.com/markbates/goth/providers/google"
	"gitlab.com/intric-dk/webservices/api/pkg/models"
)

// AuthenticationTransaction contains the information about the current authentication request.
type AuthenticationTransaction struct {
	State          string `json:"state"`
	RedirectURI    string `json:"redirect_uri"`
	OrganisationID string `json:"organisation_id"`
}

var externalURL string

// RedirectURI exposes the allowed redirect URI.
var RedirectURI string

// JWTSecret exposes the JWT secret.
var JWTSecret string

// InitExternalURL loads the external URL.
func InitExternalURL() {
	// Get external URL if the application is deployed behind a proxy.
	externalURL = Getenv("EXTERNAL_URL")
	if externalURL == "" {
		log.Fatalf("error: environment variable may not be empty: EXTERNAL_URL")
	}
}

// InitRedirectURI loads the redirect URI.
func InitRedirectURI() {
	// Get the allowed redirect URI.
	RedirectURI = Getenv("REDIRECT_URI")
	if RedirectURI == "" {
		log.Fatalf("error: environment variable may not be empty: REDIRECT_URI")
	}
}

// InitJWTSecret loads the JWT secret.
func InitJWTSecret() {
	// Get the JWT secret.
	JWTSecret = Getenv("JWT_SECRET")
	if JWTSecret == "" {
		log.Fatalf("error: environment variable may not be empty: JWT_SECRET")
	}
}

// InjectProvider injects the authentication provider name
// into the HTTP request object for the authentication library.
func InjectProvider(r *http.Request, ID string) {
	appendQuery(r, "provider", ID)
}

// LoadAuthenticationProvider ensures that the authentication provider is loaded in goth.
func LoadAuthenticationProvider(entity *models.AuthenticationProvider) error {
	// Ensure authentication provider is loaded.
	switch entity.Provider {
	case "google":
		provider := google.New(entity.ClientKey, entity.ClientSecret, callbackURL(entity.ID), "openid", "profile", "email")
		provider.SetName(entity.ID)
		goth.UseProviders(provider)
	case "gitlab":
		provider := gitlab.New(entity.ClientKey, entity.ClientSecret, callbackURL(entity.ID), "openid", "profile", "email", "read_user")
		provider.SetName(entity.ID)
		goth.UseProviders(provider)
	default:
		return errors.New("unknown authentication provider: " + entity.Provider)
	}

	return nil
}

// InjectAuthenticationTransaction sets the state to prevent CSRF attacks and to encode
// organisation and redirect URI information.
func InjectAuthenticationTransaction(r *http.Request) {
	// Get query from request.
	query := r.URL.Query()

	// Create state if it did not exist.
	state := query.Get("state")
	if len(state) == 0 {
		nonceBytes := make([]byte, 64)
		_, err := io.ReadFull(rand.Reader, nonceBytes)
		if err != nil {
			log.Fatalf("error: source of randomness unavailable: %s", err)
		}
		state = base64.URLEncoding.EncodeToString(nonceBytes)
	}

	// Create authentication transaction for information encoding in the state.
	authTx := AuthenticationTransaction{
		State:          state,
		RedirectURI:    query.Get("redirect_uri"),
		OrganisationID: query.Get("organisation_id"),
	}

	// Encode authentication transaction to JSON.
	jsonBytes, err := json.Marshal(authTx)
	if err != nil {
		appendQuery(r, "state", base64.URLEncoding.EncodeToString([]byte(state)))
	} else {
		appendQuery(r, "state", base64.URLEncoding.EncodeToString(jsonBytes))
	}
}

// ExtractAuthenticationTransaction extracts the authentication transaction from the "state" query parameter.
func ExtractAuthenticationTransaction(r *http.Request) *AuthenticationTransaction {
	// Fetch query parameter.
	state := r.URL.Query().Get("state")

	// Decode base64 encoded state.
	jsonBytes, err := base64.URLEncoding.DecodeString(state)
	if err != nil {
		return nil
	}

	// Decode JSON string.
	var authTx AuthenticationTransaction
	err = json.Unmarshal(jsonBytes, &authTx)
	if err != nil {
		return nil
	}

	return &authTx
}

// UserFromGoth will create a new user from the goth user struct.
func UserFromGoth(gothUser *goth.User) *models.User {
	firstName := gothUser.FirstName
	lastName := gothUser.LastName

	if firstName == "" || lastName == "" {
		nameSegments := strings.Split(gothUser.Name, " ")
		lastNameIndex := len(nameSegments) - 1
		firstName = strings.Join(nameSegments[:lastNameIndex], "")
		lastName = nameSegments[lastNameIndex]
	}

	// Create new user struct.
	user := models.User{
		FirstName: firstName,
		LastName:  lastName,
		Email:     gothUser.Email,
		AvatarURL: gothUser.AvatarURL,
	}

	return &user
}

// callbackURL creates a callback URL based on the authentication
// provider ID and the external URL.
func callbackURL(ID string) string {
	return externalURL + "/v1/authentication_providers/" + ID + "/redirects/authenticate"
}

// appendQuery appends a value to the query string.
func appendQuery(r *http.Request, key string, value string) {
	query := r.URL.Query()
	query.Add(key, value)
	r.URL.RawQuery = query.Encode()
}
