package util

const (
	// ReasonValidationFailed occurs when the request body field validation fails.
	ReasonValidationFailed = "Validation Failed"
	// ReasonQueryFailed occurs when the SQL query fails.
	ReasonQueryFailed = "Query Failed"
	// ReasonJSONInvalid occurs when the JSON payload cannot be parsed.
	ReasonJSONInvalid = "JSON Invalid"
	// ReasonEntityExists occurs when there is a duplicate.
	ReasonEntityExists = "Entity Exists"
	// ReasonEntityUnknown occurs when an entity cannot be found.
	ReasonEntityUnknown = "Entity Unknown"
	// ReasonEndpointUnsupported occurs when the endpoint is not part of the specification.
	ReasonEndpointUnsupported = "Endpoint Unsupported"
	// ReasonTokenExpired occurs if the authentication token is expired.
	ReasonTokenExpired = "Token Expired"
	// ReasonIATClaimInvalid occurs if the authentication token has an invalid "issued at" claim.
	ReasonIATClaimInvalid = "IAT Claim Invalid"
	// ReasonNBFClaimInvalid occurs if the authentication token has an invalid "not before" claim.
	ReasonNBFClaimInvalid = "NBF Claim Invalid"
	// ReasonUserIDClaimInvalid occurs if the authentication token has an invalid "user_id" claim.
	ReasonUserIDClaimInvalid = "User ID Claim Invalid"
	// ReasonOrganisationIDClaimInvalid occurs if the authentication token has an invalid "organisation_id" claim.
	ReasonOrganisationIDClaimInvalid = "Organisation ID Claim Invalid"
	// ReasonAlgorithmInvalid occurs if the authentication token uses the wrong signing algorithm.
	ReasonAlgorithmInvalid = "Algorithm Invalid"
	// ReasonSignatureInvalid occurs if the token has an invalid signature.
	ReasonSignatureInvalid = "Signature Invalid"
	// ReasonPolicyCheckFailed occurs if the enforcer returns an error.
	ReasonPolicyCheckFailed = "Policy Check Failed"
	// ReasonInsufficientPrivileges occurs if the user has no access to a specific resource.
	ReasonInsufficientPrivileges = "Insufficient Privileges"
	// ReasonProviderUnknown occurs when the authentication provider type is unknown.
	ReasonProviderUnknown = "Provider Unknown"
	// ReasonSessionMissing occurs if the user cannot be loaded from the session.
	ReasonSessionMissing = "Session Missing"
	// ReasonStateInvalid occurs if the authentication transaction cannot be extracted.
	ReasonStateInvalid = "State Invalid"
	// ReasonOrganisationIDMissing occurs if the organisation ID is not provided.
	ReasonOrganisationIDMissing = "Organisation ID Missing"
	// ReasonRedirectURIInvalid occurs if the redirect URI is not allowed or empty.
	ReasonRedirectURIInvalid = "Redirect URI Invalid"
	// ReasonIssuingJWTFailed occurs if the JWT cannot be issued.
	ReasonIssuingJWTFailed = "Issuing JWT Failed"
)
