package util

// Remove removes an array element without keeping the original order.
func Remove(s []string, i int) []string {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}
