package util

// ContextKey is a datatype to create context keys.
type ContextKey string

// String casts the context key to a string.
func (key *ContextKey) String() string {
	return "gitlab.com/intric-dk/webservices/api." + string(*key)
}
