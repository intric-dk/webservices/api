package util

var version string

// SetVersion configures the application version.
func SetVersion(v string) {
	version = v
}

// GetVersion returns the application version.
func GetVersion() string {
	return version
}
