# API

A REST API for the data of the Intric platform.

The API documentation can be found [here](https://intric-dk.gitlab.io/webservices/api).

## Development

Make sure to have at least Go version `1.13` installed and create a new `.env` file with the following content:

```ini
# Define the internal port for the HTTP server.
PORT=8080
# Define the secret used for JWT signing and verification.
JWT_SECRET=test
# Define from which error code the HTTP server should log.
LOGLEVEL=400
# Define the secret used of session encryption.
SESSION_SECRET=test
# Define external protocol and host.
EXTERNAL_URL=http://localhost:8080
# Allowed redirect URI.
REDIRECT_URI=http://localhost:3000/-/login
```

Additionally add one of the following lines based on your database:

```ini
# PostgreSQL
DB_URL=postgres://postgres:postgres@localhost:5432/api?sslmode=disable

# MariaDB
DB_URL=mariadb://root:mariadb@localhost:3306/api?charset=utf8&parseTime=true

# MySQL
DB_URL=mysql://root:mysql@localhost:3306/api?charset=utf8&parseTime=true
```

The application supports valid connection URLs for `mysql`, `mariadb` and `postgres`.

Afterwards, run `make` to run the application in development mode.

## Deployment

To seed the database, you can supply seed data. By default, the application will use the value of the `SEED` environment variable, which should be a JSON string as described below. Note that for legibility, the JSON seed data below is pretty-printed. If the data is supplied by value, it should be a single-line JSON string. Alternatively, a file URL in the form of `file:///absolute/path/to/seed` may also be supplied to the `SEED` environment variable and the application will seed the contents of the specified file.

```json
{
  "authorization": {
    "policies": [
      "platform_admin, *, *, *, *, allow",
      "anonymous, *, GET, rest:///v1/authentication_providers/*/redirects/*, *, allow",
      "anonymous, *, GET, rest:///v1/authentication_providers, *, allow",
      "anonymous, *, GET, rest:///v1/authentication_providers, client_key, deny",
      "anonymous, *, GET, rest:///v1/authentication_providers, client_secret, deny",
      "anonymous, *, GET, rest:///v1/authentication_providers/*, *, allow",
      "anonymous, *, GET, rest:///v1/authentication_providers/*, client_key, deny",
      "anonymous, *, GET, rest:///v1/authentication_providers/*, client_secret, deny",
      "anonymous, *, GET, rest:///v1/organisations, *, allow",
      "anonymous, *, GET, rest:///v1/organisations/*, *, allow",
      "anonymous, *, GET, rest:///v1/organisations/*/projects, *, allow",
      "anonymous, *, GET, rest:///v1/projects, *, allow",
      "anonymous, *, GET, rest:///v1/projects/*, *, allow"
    ],
    "global_roles": [
      "anonymous, anonymous",
      "f1f38cbf-a429-4e26-baac-9098a52989c0, platform_admin"
    ]
  },
  "users": [
    {
      "id": "f1f38cbf-a429-4e26-baac-9098a52989c0",
      "email": "nicklas.frahm@gmail.com",
      "first_name": "Nicklas",
      "last_name": "Frahm",
      "avatar_url": ""
    }
  ],
  "authentication_providers": [
    {
      "id": "a5104461-7d26-4dac-bbca-4e141d726cb0",
      "name": "GitLab",
      "slug": "gitlab",
      "client_key": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
      "client_secret": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
      "provider": "gitlab",
      "global": true
    },
    {
      "id": "bddf4346-c8a0-4da0-8e42-0639a12d79ed",
      "name": "Google",
      "slug": "google",
      "client_key": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
      "client_secret": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
      "provider": "google",
      "global": true
    }
  ]
}
```

## License

This project is licensed under the terms of the [MIT license](./LICENSE.md).
