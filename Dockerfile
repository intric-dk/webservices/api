# Set CPU architecture.
ARG ARCH=amd64

# Define base image for build stage.
FROM balenalib/amd64-alpine-golang:latest-build AS build

# Pull ARCH build variable into build stage.
ARG ARCH

# Configure current working directory.
WORKDIR /app

# Install dependencies.
RUN install_packages upx

# Copy source files.
COPY . ./

# Get version number.
ARG VERSION

# Build binary.
RUN make build
RUN upx -9 api-$ARCH

# Define base image for final Docker image.
FROM balenalib/$ARCH-alpine:latest-run AS run

# Pull ARCH build variable into run stage.
ARG ARCH

# Configure current working directory.
WORKDIR /app

# Configure application user.
RUN addgroup -S app && adduser -S app -G app --home "$(pwd)" --no-create-home
USER app:app

# Define environment variables.
ENV PORT=8080
ENV JWT_SECRET=""
ENV DB_URL=""
ENV SEED=""
ENV LOGLEVEL=""
ENV SESSION_SECRET=""
ENV EXTERNAL_URL=""
ENV REDIRECT_URI=""

# Expose HTTP port.
EXPOSE $PORT

# Copy binary from build stage.
COPY --from=build /app/api-$ARCH api

# Define command to start the application.
CMD [ "./api" ]
